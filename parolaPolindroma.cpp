#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int palindromo(char parola[] , int i , int f);

main(){
	int i=0, f=0;
	char parola[20];
	printf("ins una parola: ");
	scanf("%s",parola);
	f=strlen(parola);
	f--;
	palindromo(parola,i,f);
}

int palindromo(char parola[] , int i , int f){
	if(i!=f){
		if(parola[i]==parola[f]){
			i++;
			f--;
			palindromo(parola,i,f);
		}
		else{
			printf("parola non palindroma\n");
			return 0;
		}
	}
	else{
		printf("parola palindroma\n");
		return 1;
	}	
}
